SSHCOMMAND_URL ?= https://raw.github.com/progrium/sshcommand/master/sshcommand
HOOK           ?= /opt/cidokku/cidokku.git
CIDOKKU        ?= cidokku
JOLICI         ?= https://github.com/jolicode/JoliCi/releases/download/v0.3.1/jolici.phar


install: docker sshcommand jolici cidokkusetting

docker:
	wget -qO- https://get.docker.com/ | sh

sshcommand:
	wget -qO /usr/local/bin/sshcommand ${SSHCOMMAND_URL}
	chmod +x /usr/local/bin/sshcommand
	sshcommand create cidokku /usr/local/bin/${USER}

jolici:
	wget ${JOLICI} && mv jolici.phar container/www

cidokkusetting:
	mkdir -p ${HOOK}
	cd ${HOOK} && git init --bare
	cp hooks/post-receive ${HOOK}/hooks/
	chmod +x ${HOOK}/hooks/post-receive
	cp -r container docker.sh /home/cidokku
	chmod +x /home/cidokku/docker.sh
	chown -R ${CIDOKKU}:${CIDOKKU} /opt/
	chown -R ${CIDOKKU}:${CIDOKKU} /home/cidokku
